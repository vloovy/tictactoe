import copy
import random

DEFAULT_VALUE = " "

def initialize_board():
    return ({"top_L": DEFAULT_VALUE, "top_M": DEFAULT_VALUE, "top_R": DEFAULT_VALUE,
                 "middle_L": DEFAULT_VALUE, "middle_M": DEFAULT_VALUE, "middle_R": DEFAULT_VALUE,
                 "bottom_L": DEFAULT_VALUE, "bottom_M": DEFAULT_VALUE, "bottom_R": DEFAULT_VALUE})


def draw_board(board):
    print(board["top_L"] + "|" + board["top_M"] + "|" + board["top_R"] + "\n"
          + "-----\n"
          + board["middle_L"] + "|" + board["middle_M"] + "|" + board["middle_R"] + "\n"
          + "-----\n"
          + board["bottom_L"] + "|" + board["bottom_M"] + "|" + board["bottom_R"] + "\n")


def input_prompt(board):
    input_message = "Please enter one of the following field names: \n"
    for key, value in board.items():
        if value == DEFAULT_VALUE:
            input_message += key + " "
    return input_message


def who_goes_first():
    if random.randint(0, 1) == 0:
        return "computer"
    else:
        return "player"


def pick_x_or_o():
    print("Do you want to be X or O?")
    answer = input().upper()
    while answer != "X" and answer != "O":
        answer = input("Try again").upper()
    if answer == "X":
        return "X", "O"
    else:
        return "O", "X"


def is_field_available(board, field):
    if field in board and board[field] == DEFAULT_VALUE:
        return True
    else:
        return False


def make_move(board, field, symbol):
    board[field] = symbol


def player_move(board, player_symbol):
    field = ""
    while not is_field_available(board, field):
        field = input(input_prompt(board))
    make_move(board, field, player_symbol)


def choose_random_move_from_list(board, move_list):
    possible_move = []
    for i in move_list:
        if is_field_available(board, i):
            possible_move.append(i)

    if len(possible_move) != 0:
        return random.choice(possible_move)
    else:
        return None


def next_computer_move(board, computer_symbol):
    if computer_symbol == "X":
        player_symbol = "O"
    else:
        player_symbol = "X"

    # check if computer can win this move
    for i in board.keys():
        board_copy = copy.copy(board)
        if is_field_available(board_copy, i):
            make_move(board_copy, i, computer_symbol)
            if is_three_in_a_row(board_copy, computer_symbol)[0]:
                return i

    # check if player can win next move
    for i in board.keys():
        board_copy = copy.copy(board)
        if is_field_available(board_copy, i):
            make_move(board_copy, i, player_symbol)
            if is_three_in_a_row(board_copy, player_symbol)[0]:
                return i

    # try to take middle
    if is_field_available(board, "middle_M"):
        return "middle_M"

    # move to corner if possible
    move = choose_random_move_from_list(board, ["top_L", "top_R", "bottom_L", "bottom_R"])
    if move is not None:
        return move

    # choose side field
    return choose_random_move_from_list(board, ["top_M", "middle_L", "middle_R", "bottom_M"])


def computer_move(board, computer_symbol):
    make_move(board, next_computer_move(board, computer_symbol), computer_symbol)


def is_three_in_a_row(board, letter):
    return (board["top_L"] == board["top_M"] == board["top_R"] == letter or
            board["middle_L"] == board["middle_M"] == board["middle_R"] == letter or
            board["bottom_L"] == board["bottom_M"] == board["bottom_R"] == letter or
            board["top_L"] == board["middle_L"] == board["bottom_L"] == letter or
            board["top_M"] == board["middle_M"] == board["bottom_M"] == letter or
            board["top_R"] == board["middle_R"] == board["bottom_R"] == letter or
            board["top_L"] == board["middle_M"] == board["bottom_R"] == letter or
            board["top_R"] == board["middle_M"] == board["bottom_L"] == letter, letter)


def end_game_message(board, player_symbol, winner_symbol):
    if is_board_full(board):
        print("It's a draw.")
    elif player_symbol == winner_symbol:
        print("Congratulations! You've won!")
    else:
        print("You've lost")


def whose_next_move(next_move):
    print("Next move: " + next_move)


def is_board_full(board):
    return " " not in board.values()


def run():
    playing_board = initialize_board()
    player_symbol, computer_symbol = pick_x_or_o()
    next_move = who_goes_first()
    is_won = False
    draw_board(playing_board)

    while not is_won and not is_board_full(playing_board):
        whose_next_move(next_move)
        if next_move == "computer":
            computer_move(playing_board, computer_symbol)
            is_won, winner_symbol = is_three_in_a_row(playing_board, computer_symbol)
            next_move = "player"
        else:
            player_move(playing_board, player_symbol)
            is_won, winner_symbol = is_three_in_a_row(playing_board, player_symbol)
            next_move = "computer"
        draw_board(playing_board)
    end_game_message(playing_board, player_symbol, winner_symbol)
    input()


if __name__=="__main__":
    run()
